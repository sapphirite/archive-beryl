use {
    std::{
        path::{PathBuf},
    },
    actix_web::{
        fs::{NamedFile},
        http::{Method},
        App, server, HttpRequest, Result
    }
};

fn index(_req: &HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("./index.html")?)
}

fn static_file(req: &HttpRequest) -> Result<NamedFile> {
    // Assemble the path relative to the dist directory
    let relative_path: PathBuf = req.match_info().query("tail")?;

    // First try the dist directory, it has priority
    let mut path = PathBuf::from("../client/dist/");
    path.push(&relative_path);

    // If we couldn't find a file in dist, instead go to static
    if !path.exists() {
        path = PathBuf::from("../static/");
        path.push(&relative_path);
    }

    let mut named_file = NamedFile::open(path)?;

    // If this is a wasm file, give it the right MIME type
    if relative_path.extension().map(|v| v == "wasm").unwrap_or(false) {
        named_file = named_file.set_content_type("application/wasm".parse().unwrap());
    }

    Ok(named_file)
}

fn main() {
    let server = server::new(|| {
        App::new()
            .resource("/", |r| r.f(index))
            .resource(r"/static/{tail:.*}", |r| r.method(Method::GET).f(static_file))
    });

    server.bind("0.0.0.0:3000")
        .expect("Can not bind to port 3000")
        .run();
}