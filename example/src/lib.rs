#![warn(clippy::all)]

mod counter;
mod todo;

use {
    beryl::{
        vdom::{VNode, VComponent, el, ComponentExt},
        Registry, Component,
    },

    crate::{
        counter::{Counter},
        todo::{Todo},
    },
};

pub fn init() -> (Registry, VComponent) {
    let mut registry = Registry::new();

    registry.add(App);
    registry.add(Counter);
    registry.add(Todo);

    (registry, App::v().into())
}

struct App;

impl Component for App {
    type State = ();

    fn state(&self) { }

    fn render(&self, _: &()) -> VNode {
        el("section")
            .children(vec!(
                el("h1").child("Hello from Rust!").into(),
                el("p").child("This is a small hello world vDOM example in Rust").into(),
                Counter::v().into(),
                Todo::v().into(),
            )).into()
    }
}
