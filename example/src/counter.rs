use {
    log::{info},
    beryl::{
        context::{Context},
        vdom::{VNode, el},
        Component, Error,
    },
};

pub struct CounterState {
    count: i32,
}

pub struct Counter;

impl Component for Counter {
    type State = CounterState;

    fn state(&self) -> CounterState {
        CounterState {
            count: 5,
        }
    }

    fn render(&self, state: &CounterState) -> VNode {
        el("section")
            .children(vec!(
                el("h2").child("Counter").into(),
                el("p").child(format!("Count: {}", state.count)).into(),
                el("button")
                    .on("click", sub_clicked)
                    .child("Sub").into(),
                el("button")
                    .on("click", add_clicked)
                    .child("Add").into()
            )).into()
    }
}

fn sub_clicked(ctx: Context<Counter>) -> Result<(), Error> {
    ctx.apply_fn(|s| s.count -= 1)?;
    info!("New counter value: {}", ctx.get(|s| s.count)?);

    Ok(())
}

fn add_clicked(ctx: Context<Counter>) -> Result<(), Error> {
    ctx.apply_fn(|s| s.count += 1)?;
    info!("New counter value: {}", ctx.get(|s| s.count)?);

    Ok(())
}