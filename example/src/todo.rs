use {
    log::{info},
    beryl::{
        vdom::{VNode, el},
        context::{Context},
        Component, Error,
    },
};

pub struct TodoState {
    items: Vec<Item>,
    last_key: usize,
}

struct Item {
    text: String,
    done: bool,
    key: usize,
}

pub struct Todo;

impl Component for Todo {
    type State = TodoState;

    fn state(&self) -> TodoState {
        TodoState {
            items: vec!(
                Item {
                    text: "Read this entry".to_string(),
                    done: true,
                    key: 0,
                },
                Item {
                    text: "Mark this entry as done".to_string(),
                    done: false,
                    key: 1,
                }
            ),
            last_key: 1,
        }
    }

    fn render(&self, state: &TodoState) -> VNode {
        let items = state.items.iter().enumerate().map(|(i, item)| {
            let mut checkbox = el("input").attr("type", "checkbox");
            if item.done {
                checkbox = checkbox.flag("checked");
            }

            el("li")
                .key(item.key)
                .children(vec!(
                    checkbox.into(),
                    " ".into(),
                    (&item.text).into(),
                    " ".into(),
                    el("button")
                        .on("click", move |ctx| remove(ctx, i))
                        .child("Remove").into(),
                    el("input").into(),
                )).into()
        }).collect();

        el("section")
            .children(vec!(
                el("h2").child("To-Do").into(),
                el("input").into(),
                " ".into(),
                el("button")
                    .on("click", add)
                    .child("Add").into(),
                el("ul")
                    .keyed()
                    .children(items).into(),
            )).into()
    }
}

fn add(ctx: Context<Todo>) -> Result<(), Error> {
    let key = ctx.get(|s| s.last_key)? + 1;
    let item = Item {
        text: "item text".to_string(),
        done: false,
        key,
    };

    ctx.apply_fn(move |s| {
        s.items.push(item);
        s.last_key = key;
    })?;

    Ok(())
}

fn remove(ctx: Context<Todo>, i: usize) -> Result<(), Error> {
    info!("Removing item {}", i);

    ctx.apply_fn(|s| { s.items.remove(i); })?;

    Ok(())
}