use {
    log::{Level},
    wasm_bindgen::prelude::*,
    beryl_dom::{hydrate},
};

#[wasm_bindgen]
pub fn bootstrap() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(Level::Debug).unwrap();

    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let container = document.get_element_by_id("app").unwrap();

    let (registry, root) = example::init();

    hydrate(&root, registry, &container);
}
