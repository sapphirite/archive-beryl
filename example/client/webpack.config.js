const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");

module.exports = {
    entry: "./index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "index.js",
        chunkFilename: "[name].chunk.js",
        publicPath: "/static/"
    },
    plugins: [
        new CleanWebpackPlugin(["dist/*"]),
        new WasmPackPlugin({
            crateDirectory: path.resolve(__dirname, "."),
        })
    ],
    mode: "development"
};