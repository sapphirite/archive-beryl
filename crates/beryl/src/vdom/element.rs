use {
    std::{
        any::{Any, TypeId},
        collections::{HashMap},
        rc::{Rc},
    },
    crate::{
        vdom::{VNode},
        StoredHandler, EventHandler, Component,
    },
};

#[derive(Clone, Debug)]
pub struct VElement {
    pub tag: String,
    pub key: Option<String>,
    pub attributes: HashMap<String, String>,
    pub events: HashMap<String, VEventHandler>,
    /// If true this element's children should be diffed using the keyed algorithm.
    pub keyed: bool,
    pub children: Vec<VNode>,
}

impl VElement {
    pub fn new<S: Into<String>>(tag: S) -> Self {
        VElement {
            tag: tag.into(),
            key: None,
            attributes: HashMap::new(),
            children: Vec::new(),
            keyed: false,
            events: HashMap::new(),
        }
    }

    /// Elements given a key are persisted according to their key in the DOM.
    /// They're moved around rather than overwritten.
    pub fn key<K: ToString>(mut self, key: K) -> Self {
        self.key = Some(key.to_string());
        self
    }

    pub fn attr<K: Into<String>, V: Into<String>>(mut self, key: K, value: V) -> Self {
        let key = key.into();
        let value = value.into();

        if self.attributes.contains_key(&key) {
            // TODO: Make this a recoverable error
            panic!("Attribute \"{}\" already added", key)
        }

        self.attributes.insert(key, value);

        self
    }

    pub fn flag<K: Into<String>>(self, key: K) -> Self {
        self.attr(key, "")
    }

    pub fn on<O: Into<String>, C: Component, H: EventHandler<C>>(
        mut self, on: O, handler: H
    ) -> Self {
        let on = on.into();

        if self.events.contains_key(&on) {
            // TODO: Make this a recoverable error
            panic!("Event \"{}\" already registered", on)
        }

        let event = VEventHandler {
            handler: Rc::new(StoredHandler { handler: Rc::new(handler) }),
            type_id: TypeId::of::<H>(),
        };
        self.events.insert(on, event);
        self
    }

    /// Marks that this element's children should be diffed using the keyed algorithm.
    pub fn keyed(mut self) -> Self {
        self.keyed = true;
        self
    }

    /// Appends a child to the end of the element's children.
    pub fn child<N: Into<VNode>>(mut self, child: N) -> Self {
        self.children.push(child.into());
        self
    }

    /// Appends multiple children to the end of the element's children.
    pub fn children(mut self, children: Vec<VNode>) -> Self {
        self.children.extend(children);
        self
    }
}

impl From<VElement> for VNode {
    fn from(element: VElement) -> VNode {
        VNode::Element(element)
    }
}

#[derive(Clone, Debug)]
pub struct VEventHandler {
    pub handler: Rc<Any>,
    pub type_id: TypeId,
}

/// Shorthand for `VElement::new`.
#[inline]
pub fn el<S: Into<String>>(tag: S) -> VElement {
    VElement::new(tag)
}
