mod component;
mod element;

pub use self::{
    component::{VComponent, VComponentBuilder, ComponentExt},
    element::{VElement, VEventHandler, el},
};

#[derive(Clone, Debug)]
pub enum VNode {
    Element(VElement),
    Component(VComponent),
    Text(String),
    /// A placeholder non-node. Will not be rendered to the DOM as an element, but maintains a slot
    /// in the vDOM for cheaper diffing of insertions between components.
    None,
}

impl<'a> From<&'a str> for VNode {
    fn from(text: &'a str) -> VNode {
        VNode::Text(text.into())
    }
}

impl From<String> for VNode {
    fn from(text: String) -> VNode {
        VNode::Text(text)
    }
}

impl From<&String> for VNode {
    fn from(text: &String) -> VNode {
        VNode::Text(text.clone())
    }
}