use {
    std::{
        any::{Any, TypeId},
        marker::{PhantomData},
    },
    crate::{vdom::{VNode}, Component},
};

#[derive(Clone, Debug)]
pub struct VComponent {
    pub type_id: TypeId,
}

impl From<VComponent> for VNode {
    fn from(component: VComponent) -> VNode {
        VNode::Component(component)
    }
}

/// Adds `node` convenience wrapper on types implementing `Component`.
pub trait ComponentExt: Sized {
    /// Creates a type-safe `VComponentBuilder` for this component.
    /// You can turn this builder into a `VComponent` or `VNode::Component` using `.into()`.
    fn v() -> VComponentBuilder<Self>;
}

impl<C: Component> ComponentExt for C {
    fn v() -> VComponentBuilder<C> {
        VComponentBuilder {
            _c: PhantomData::default(),
        }
    }
}

/// Type-safe helper to construct a `VComponent` for a typed component.
pub struct VComponentBuilder<C> {
    _c: PhantomData<C>,
}

impl<C: Any> From<VComponentBuilder<C>> for VComponent {
    fn from(_builder: VComponentBuilder<C>) -> VComponent {
        let type_id = TypeId::of::<C>();

        VComponent {
            type_id
        }
    }
}

impl<C: Any> From<VComponentBuilder<C>> for VNode {
    fn from(builder: VComponentBuilder<C>) -> VNode {
        VNode::Component(builder.into())
    }
}