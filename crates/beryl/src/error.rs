#[derive(Debug)]
pub enum Error {
    ComponentDropped,
    ComponentInstanceDropped,
    EventHandlerWrongTypes,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::ComponentDropped =>
                write!(f,
                    "The component associated with a component instance couldn't be accessed, \
                    because it was dropped."
                ),
            Error::ComponentInstanceDropped =>
                write!(f,
                    "The component instance associated with an executor couldn't be accessed, \
                    because it was dropped."
                ),
            Error::EventHandlerWrongTypes =>
                write!(f, "Event handler has the wrong argument types."),
        }
    }
}

impl std::error::Error for Error {
}
