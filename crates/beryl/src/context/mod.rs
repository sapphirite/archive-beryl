use {
    std::{
        rc::{Rc, Weak},
        cell::{RefCell},
    },

    crate::{Component, Error},
};

/// A context wrapper for lifecycle and browser events.
pub struct Context<C: Component> {
    component: Weak<C>,
    state: Weak<RefCell<C::State>>,
    listener: Box<ContextListener>,
}

impl<C: Component> Context<C> {
    pub fn new(
        component: Weak<C>, state: Weak<RefCell<C::State>>, listener: Box<ContextListener>,
    ) -> Self {
        Self {
            component,
            state,
            listener,
        }
    }

    pub fn component(&self) -> Result<Rc<C>, Error> {
        self.component.upgrade()
            .ok_or(Error::ComponentDropped)
    }

    pub fn apply<A: StateApply<C::State>>(&self, apply: A) -> Result<(), Error> {
        let state = self.state.upgrade()
            .ok_or(Error::ComponentInstanceDropped)?;

        // Actually apply the state change
        apply.apply(&mut state.borrow_mut());

        // Signal to the listener that the state has changed, so the DOM can be updated
        self.listener.state_changed();

        Ok(())
    }

    /// Helper for when S can't be inferred on `StateApply` on closures.
    pub fn apply_fn<F: FnOnce(&mut C::State)>(&self, apply: F) -> Result<(), Error> {
        self.apply(apply)
    }

    pub fn get<O, F: FnOnce(&C::State) -> O>(&self, getter: F) -> Result<O, Error> {
        let state = self.state.upgrade()
            .ok_or(Error::ComponentInstanceDropped)?;

        let value = getter(&state.borrow());

        Ok(value)
    }
}

/// Implements DOM updates after state updates have happened.
pub trait ContextListener {
    fn state_changed(&self);
}

/// Applies a change to a state.
pub trait StateApply<S> {
    fn apply(self, state: &mut S);
}

impl<S, F: FnOnce(&mut S)> StateApply<S> for F {
    fn apply(self, state: &mut S) {
        (self)(state)
    }
}
