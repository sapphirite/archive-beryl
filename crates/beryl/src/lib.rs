#![warn(clippy::all)]

pub mod context;
pub mod vdom;

mod error;
mod instance;
mod registry;

pub use self::{
    error::{Error},
    registry::{Registry, ComponentGeneric},
    instance::{ComponentInstanceGeneric, ExecutorGeneric},
};

use {
    std::{
        any::{Any},
        rc::{Rc},
    },

    crate::{
        context::{Context},
        vdom::{VNode},
    },
};

pub trait Component: Any {
    type State;

    fn state(&self) -> Self::State;
    fn render(&self, state: &Self::State) -> VNode;
}

/// A handler for events raised by elements or components.
pub trait EventHandler<C: Component>: 'static {
    /// Called when the event has been triggered.
    fn handle(&self, ctx: Context<C>) -> Result<(), Error>;
}

impl<C: Component, F: Fn(Context<C>) -> Result<(), Error> + 'static> EventHandler<C> for F {
    fn handle(&self, ctx: Context<C>) -> Result<(), Error> {
        (self)(ctx)
    }
}

/// Wrapper around handlers that makes it easier to store them as Any and retrieve them again.
struct StoredHandler<C> {
    pub handler: Rc<EventHandler<C>>,
}

// Reminder for how to compare handler types, the TypeId has to be taken *immediately* when it's
// registered from the generic. This makes it the easiest to get the comparison right.
#[cfg(test)]
mod tests {
    use {
        std::any::{TypeId},
        crate::{
            context::{Context},
            vdom::{VNode},
            Component, EventHandler, Error,
        },
    };

    #[test]
    fn same_function_matches() {
        let a = get_type(func_1);
        let b = get_type(func_1);
        assert!(a == b);
    }

    #[test]
    fn different_function_doesnt_match() {
        let a = get_type(func_1);
        let b = get_type(func_2);
        assert!(a != b);
    }

    fn get_type<T: EventHandler<TestComponent>>(_value: T) -> TypeId {
        TypeId::of::<T>()
    }

    struct TestComponent;

    impl Component for TestComponent {
        type State = ();

        fn state(&self) { }

        fn render(&self, _state: &()) -> VNode { unimplemented!() }
    }

    fn func_1(_ctx: Context<TestComponent>) -> Result<(), Error> { Ok(()) }
    fn func_2(_ctx: Context<TestComponent>) -> Result<(), Error> { Ok(()) }
}