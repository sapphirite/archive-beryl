use {
    std::{
        any::{Any},
        cell::{RefCell},
        rc::{Rc, Weak},
    },
    crate::{
        context::{ContextListener},
        vdom::{VNode},
        Component, Error, StoredHandler, Context, EventHandler,
    },
};

/// An instance of a component.
pub trait ComponentInstanceGeneric {
    fn render(&self) -> Result<VNode, Error>;

    /// Creates an event handler executor that can be called without keeping ownership of this
    /// type. Performs type checking once, the handler can be called multiple times without
    /// further type checking.
    fn create_executor(&self, handler: &Any) -> Result<Box<ExecutorGeneric>, Error>;
}

pub struct ComponentInstance<C: Component> {
    component: Weak<C>,
    state: Rc<RefCell<C::State>>,
}

impl<C: Component> ComponentInstance<C> {
    pub fn new(component: Weak<C>, state: C::State) -> Self {
        Self {
            component,
            state: Rc::new(RefCell::new(state)),
        }
    }
}

impl<C: Component> ComponentInstanceGeneric for ComponentInstance<C> {
    fn render(&self) -> Result<VNode, Error> {
        let component = self.component.upgrade()
            .ok_or(Error::ComponentDropped)?;

        let vroot = component.render(&self.state.borrow());

        Ok(vroot)
    }

    fn create_executor(&self, handler: &Any) -> Result<Box<ExecutorGeneric>, Error> {
        let stored = handler.downcast_ref::<StoredHandler<C>>()
            .ok_or(Error::EventHandlerWrongTypes)?;
        
        let executor = Executor {
            component: self.component.clone(),
            state: Rc::downgrade(&self.state),
            handler: stored.handler.clone(),
        };
        Ok(Box::new(executor))
    }
}

pub trait ExecutorGeneric {
    fn call(&self, applicator: Box<ContextListener>) -> Result<(), Error>;
}

struct Executor<C: Component> {
    component: Weak<C>,
    state: Weak<RefCell<C::State>>,
    handler: Rc<EventHandler<C>>,
}

impl<C: Component> ExecutorGeneric for Executor<C> {
    fn call(&self, listener: Box<ContextListener>) -> Result<(), Error> {
        let context = Context::new(
            self.component.clone(),
            self.state.clone(),
            listener,
        );

        self.handler.handle(context)
    }
}