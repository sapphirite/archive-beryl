use {
    std::{
        any::{TypeId},
        collections::{HashMap},
        rc::{Rc},
    },

    log::{debug},

    crate::{
        instance::{ComponentInstance},
        Component, ComponentInstanceGeneric,
    },
};

#[derive(Default)]
pub struct Registry {
    components: HashMap<TypeId, Box<ComponentGeneric>>,
}

impl Registry {
    pub fn new() -> Self {
        Registry {
            components: HashMap::new(),
        }
    }

    pub fn add<C: Component>(&mut self, component: C) {
        let component = Rc::new(component);
        self.components.insert(TypeId::of::<C>(), Box::new(component));
    }
    
    pub fn get(&self, type_id: TypeId) -> Option<&ComponentGeneric> {
        self.components.get(&type_id).map(AsRef::as_ref)
    }
}

/// A registered wrapped component in the registry.
pub trait ComponentGeneric {
    fn create(&self) -> Box<ComponentInstanceGeneric>;
}

impl<C: Component> ComponentGeneric for Rc<C> {
    fn create(&self) -> Box<ComponentInstanceGeneric> {
        debug!("Creating component instance");

        let instance = ComponentInstance::new(Rc::downgrade(&self), self.state());
        Box::new(instance)
    }
}
