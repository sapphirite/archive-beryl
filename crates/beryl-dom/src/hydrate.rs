//! Rendering vDOM trees and components into existing DOM trees.

use {
    log::{info},
    web_sys::{Element},

    beryl::{
        vdom::{VComponent},
        Registry,
    },

    crate::{
        render::{render_component},
        instance::{BerylInstance, BerylHandle},
    },
};

/// Initializes a Beryl component into an empty container, or server-side generated HTML.
/// Matches the vDOM to the real DOM, attaching events and patching differences where necessary.
pub fn hydrate(root: &VComponent, registry: Registry, container: &Element) {
    info!("Hydrating Beryl Component");

    // TODO: Actually diff against a SSRed HTML

    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();

    let instance = BerylInstance::new(document, registry);
    let handle = BerylHandle::new(instance);

    let dom = render_component(root, &mut handle.state.borrow_mut(), &handle);
    container.append_child(&dom).unwrap();

    info!("Finished Hydrating Beryl Component");
}
