#![warn(clippy::all)]

mod diff;
mod event;
mod hydrate;
mod instance;
mod render;

pub use self::{
    hydrate::{hydrate},
};

use {
    log::{error},
    web_sys::{Node},
    beryl::vdom::{VNode},
};

fn to_node<N: AsRef<Node>>(node: &N) -> Node {
    node.as_ref().clone()
}

fn verify_no_key(node: &VNode) {
    if let VNode::Element(element) = node {
        if element.key.is_some() {
            error!(
                "An element not marked to use the keyed children diffing algorithm, has a keyed \
                child. The key will be ignored."
            );
        }
    }
}

fn get_key_with_error(node: &VNode) -> Option<&String> {
    if let Some(key) = get_key(node) {
        Some(key)
    } else {
        error!(
            "An element marked to use the keyed children diffing algorithm, has a non-keyed \
            child. The child will be ignored."
        );
        None
    }
}

fn get_key(node: &VNode) -> Option<&String> {
    if let VNode::Element(element) = node {
        if let Some(key) = &element.key {
            Some(key)
        } else {
            None
        }
    } else {
        None
    }
}