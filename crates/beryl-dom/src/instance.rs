use {
    std::{
        rc::{Rc},
        cell::{RefCell},
    },

    log::{debug},
    wasm_bindgen::closure::{Closure},
    web_sys::{Document, Node},
    slotmap::{SlotMap, SecondaryMap, DefaultKey},

    beryl::{
        vdom::{VNode},
        ComponentInstanceGeneric, Registry,
    },
};

pub struct BerylInstance {
    pub document: Document,
    pub registry: Registry,
    pub state: RefCell<State>,
}

impl BerylInstance {
    pub fn new(document: Document, registry: Registry) -> Self {
        Self {
            document,
            registry,
            state: RefCell::new(State::new()),
        }
    }
}

pub type BerylHandle = Rc<BerylInstance>;

pub struct State {
    // SlotMap requires Copy, so put the actual components into a SecondaryMap
    keys: SlotMap<DefaultKey, ()>,
    components: SecondaryMap<DefaultKey, ActiveComponent>,
}

impl State {
    pub fn new() -> Self {
        Self {
            keys: SlotMap::new(),
            components: SecondaryMap::new(),
        }
    }

    pub fn add_active(&mut self, active: ActiveComponent) -> DefaultKey {
        debug!("Adding active component to state");

        let key = self.keys.insert(());
        self.components.insert(key, active);

        key
    }

    pub fn active_mut(&mut self, key: DefaultKey) -> Option<&mut ActiveComponent> {
        self.components.get_mut(key)
    }
}

pub struct ActiveComponent {
    pub instance: Box<ComponentInstanceGeneric>,
    pub root: Option<Node>,
    pub vroot: Option<VNode>,
    pub handlers: Vec<Closure<Fn()>>,
}