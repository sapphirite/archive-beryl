//! Rendering vDOM trees and components onto existing DOM trees backed by a vDOM.

use {
    std::collections::{HashMap},

    log::{debug, error},
    wasm_bindgen::{JsCast},
    web_sys::{Node, Text},
    slotmap::{DefaultKey},

    beryl::vdom::{VNode, VElement},

    crate::{
        instance::{State, BerylHandle},
        render::{render_node},
        verify_no_key, get_key, get_key_with_error,
    },
};

pub fn diff_and_apply_component(
    root: &Node, previous: &VNode, next: &VNode,
    instance_key: DefaultKey, state: &mut State, handle: &BerylHandle,
) {
    debug!("Diffing active component");
    diff_node(root, previous, next, instance_key, state, handle);
}

fn diff_node(
    dom: &Node, previous: &VNode, next: &VNode,
    instance_key: DefaultKey, state: &mut State, handle: &BerylHandle,
) {
    match next {
        VNode::Element(next) =>
            diff_element(dom, previous, next, instance_key, state, handle),
        VNode::Component(_component) =>
            {} // TODO: Implement
        VNode::Text(next) =>
            diff_text(dom, previous, next),
        VNode::None =>
            unexpected_none(),
    }
}

fn diff_element(
    dom: &Node, previous: &VNode, next: &VElement,
    instance_key: DefaultKey, state: &mut State, handle: &BerylHandle,
) {
    // TODO: This ignores diffing elements that weren't previously elements, instead apply the
    // difference.
    #[allow(clippy::single_match)]
    match previous {
        VNode::Element(previous) => {
            // TODO: Verify tags match
            // TODO: Diff attributes
            // TODO: Diff events
            if previous.keyed {
                diff_children_keyed(
                    dom, &previous.children, &next.children,
                    instance_key, state, handle,
                );
            } else {
                diff_children_unkeyed(
                    dom, &previous.children, &next.children,
                    instance_key, state, handle,
                );
            }
        },
        VNode::None =>
            unexpected_none(),
        // TODO: Implement replacing other nodes
        _ => unimplemented!()
    }
}

fn diff_children_unkeyed(
    parent_dom: &Node, previous: &[VNode], next: &[VNode],
    instance_key: DefaultKey, state: &mut State, handle: &BerylHandle,
) {
    // TODO: Support VNode::None previous or next here, since diff_node expects a DOM node, and
    // adding/removing nodes at the end currently doesn't take into account Nones.

    let children = parent_dom.child_nodes();

    // Diff over all nodes we can diff, those that have entries in both lists
    let min_len = previous.len().min(next.len());
    for (i, next) in next[0..min_len].iter().enumerate() {
        let previous = &previous[i];

        if let Some(dom) = children.get(i as u32) {
            verify_no_key(next);
            diff_node(&dom, previous, next, instance_key, state, handle);
        } else {
            modified_dom_error();
            if let Some(dom) = render_node(next, instance_key, state, handle) {
                parent_dom.append_child(&dom).unwrap();
            }
        }
    }

    let len_diff = next.len() as i32 - previous.len() as i32;

    // If we have more nodes than we had previously, append the additional ones
    if len_diff > 0 {
        for next in &next[min_len..] {
            verify_no_key(next);

            if let Some(dom) = render_node(next, instance_key, state, handle) {
                parent_dom.append_child(&dom).unwrap();
            }
        }
    }

    // If we have less nodes, remove the remaining ones
    if len_diff < 0 {
        let index = min_len as u32;
        let amount = children.length() as usize - min_len;
        for _ in 0..amount {
            let dom = children.get(index).unwrap();
            parent_dom.remove_child(&dom).unwrap();
        }
    }
}

fn diff_children_keyed(
    parent_dom: &Node, previous: &[VNode], next: &[VNode],
    instance_key: DefaultKey, state: &mut State, handle: &BerylHandle,
) {
    let children = parent_dom.child_nodes();

    // Create a mapping so we're only iterating over previous with keys, in case the library user
    // added previous nodes without keys.
    // These will match to DOM nodes, not previous.
    let previous_with_keys: Vec<_> = previous.iter().enumerate()
        .filter_map(|(i, p)| get_key(p).map(|k| (i, k)))
        .collect();

    // Record the keys that were available in the previous vDOM, along with associated DOM elements
    // This way we can match keyed elements to old elements.
    let mut old_keys = HashMap::new();
    for (i, key) in previous_with_keys.iter() {
        if let Some(dom) = children.get(*i as u32) {
            old_keys.insert(*key, (*i, dom));
        } else {
            // In this case, we can just treat the old element as being gone, and treat
            // what should have been a matching one like any new keyed child.
            modified_dom_error();
        }
    }

    // Metadata for the old nodes, so we can remember if one was moved, and to skip over those
    let mut prev_moved: Vec<_> = previous_with_keys.iter().map(|_| false).collect();

    // The index of the previous vDOM element matching the current DOM node.
    let mut curr_prev = 0;

    // Current DOM index, will get out of sync with `curr_prev` as we insert or move nodes.
    let mut curr_dom = 0;

    for next in next {
        // Treat nodes with no key like they do not exist
        let next_key = if let Some(key) = get_key_with_error(next) {
            key
        } else {
            continue
        };

        // Skip over moved previous
        while prev_moved.get(curr_prev).cloned().unwrap_or(false) {
            curr_prev += 1;
        }

        // If we have run out of previous to iterate over, just append the new ones
        if previous_with_keys.len() <= curr_prev {
            let child = render_node(next, instance_key, state, handle).unwrap();
            parent_dom.append_child(&child).unwrap();

            continue;
        }

        // Get the current previous we're looking at
        let (previous_i, previous_key) = previous_with_keys[curr_prev];
        let previous = &previous[previous_i];

        // Get the associated DOM node for the previous
        let dom = children.get(curr_dom as u32);

        // If we have the same node in both positions, just diff them
        if previous_key == next_key {
            // TODO: Don't hard-error, do something
            let dom = dom.unwrap();
            diff_node(&dom, previous, next, instance_key, state, handle);

            curr_prev += 1;
            curr_dom += 1;
            old_keys.remove(next_key);

            continue
        }

        if let Some((old_keys_i, old_keys_dom)) = old_keys.get(next_key) {
            // If we have a next node that is in the previous' keys, but not here, move it
            parent_dom.insert_before(old_keys_dom, dom.as_ref()).unwrap();

            curr_dom += 1;
            prev_moved[*old_keys_i] = true;
            old_keys.remove(next_key);

            continue
        } else {
            // If we have a next node that's not in the previous' keys, insert it
            let child = render_node(next, instance_key, state, handle).unwrap();
            parent_dom.insert_before(&child, dom.as_ref()).unwrap();

            curr_dom += 1;

            continue
        }
    }

    // The keys remaining in the map are ones that weren't in next, so remove them
    for (_key, (_i, dom)) in old_keys {
        debug!("REMOVING {}", _key);
        parent_dom.remove_child(&dom).unwrap();
    }
}

fn diff_text(dom: &Node, previous: &VNode, next: &str) {
    #[allow(clippy::single_match)]
    match previous {
        VNode::Text(previous) => {
            // If there's no difference, nothing to do
            if previous == next { return }

            // There is a difference, so set the text node to the new value
            // TODO: Handle difference in element errors
            let text_dom = dom.dyn_ref::<Text>().unwrap();
            text_dom.set_data(next);
        },
        VNode::None =>
            unexpected_none(),
        // TODO: Implement replacing other nodes
        _ => unimplemented!()
    }
}

fn unexpected_none() {
    panic!(
        "A `VNode::None` reached a function that can't handle Nones, this should have been \
        handled in `diff_text`."
    )
}

fn modified_dom_error() {
    error!("The DOM was modified outside Beryl and had to be patched.");
}