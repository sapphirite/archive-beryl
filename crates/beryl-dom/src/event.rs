use {
    log::{error},
    wasm_bindgen::{closure::{Closure}, JsCast},
    web_sys::{Element},
    slotmap::{DefaultKey},

    beryl::{
        context::{ContextListener},
        vdom::{VEventHandler},
        ExecutorGeneric,
    },

    crate::{
        diff::{diff_and_apply_component},
        instance::{BerylHandle, State},
        render::{try_render_instance},
    },
};

pub(crate) fn add_event_to_element(
    dom: &Element, event: &str, handler: &VEventHandler, instance_key: DefaultKey,
    state: &mut State, handle: &BerylHandle,
) {
    // `as_ref` unpacks the Rc into the underlying type, passing just `&handler` will make
    // `call_handler`'s `Any` of type `Rc<Any>`.
    let result = state.active_mut(instance_key).unwrap()
        .instance.create_executor(handler.handler.as_ref());

    // Just ignore the event if it's not correctly typed, don't break the entire frontend
    let executor = match result {
        Ok(executor) => executor,
        Err(error) => {
            error!("Couldn't bind event \"{}\":\n{}", event, error);
            return
        }
    };

    // Create the callback closure that will be called on the event
    let closure = create_closure(
        event.to_string(),
        executor,
        instance_key,
        handle.clone(),
    );

    // TODO: Use event delegation, hook event listeners to the container or document, and
    // manually delegate them to the right handler.

    // Bind the event to the DOM
    dom.add_event_listener_with_callback(event, closure.as_ref().unchecked_ref())
        .unwrap();

    // Store the event on this component so it's not dropped
    let active = state.active_mut(instance_key).unwrap();
    active.handlers.push(closure);
}

fn create_closure(
    event: String, executor: Box<ExecutorGeneric>,
    instance_key: DefaultKey, handle: BerylHandle,
) -> Closure<Fn()> {
    let boxed: Box<Fn()> = Box::new(move || {
        let applicator = DomContextListener {
            handle: handle.clone(),
            instance_key,
        };

        let result = executor.call(Box::new(applicator));

        if let Err(error) = result {
            error!("Event handler for \"{}\" failed:\n{}", event, error);
        }
    });
    Closure::wrap(boxed)
}

struct DomContextListener {
    handle: BerylHandle,
    instance_key: DefaultKey,
}

impl ContextListener for DomContextListener {
    fn state_changed(&self) {
        let mut state = self.handle.state.borrow_mut();

        let (root, vroot, new_vroot) = {
            let active = state.active_mut(self.instance_key).unwrap();

            // Render the component with its new state
            let new_vroot = match try_render_instance(active.instance.as_ref()) {
                Ok(vroot) => vroot,
                Err(()) => return
            };

            (active.root.clone().unwrap(), active.vroot.take().unwrap(), new_vroot)
        };

        // Diff the vDOM and apply changes
        diff_and_apply_component(
            &root, &vroot, &new_vroot,
            self.instance_key, &mut state, &self.handle,
        );

        // Save the new rendered vroot
        let active = state.active_mut(self.instance_key).unwrap();
        active.vroot = Some(new_vroot);
    }
}