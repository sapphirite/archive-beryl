//! Rendering vDOM trees and components into new DOM trees.

use {
    log::{error},
    web_sys::{Node},
    slotmap::{DefaultKey},

    beryl::{
        vdom::{VNode, VElement, VComponent, el},
        ComponentInstanceGeneric,
    },

    crate::{
        event::{add_event_to_element},
        instance::{BerylHandle, ActiveComponent, State},
        to_node, verify_no_key, get_key_with_error,
    },
};

pub(crate) fn render_node(
    vnode: &VNode, instance_key: DefaultKey,
    state: &mut State, handle: &BerylHandle,
) -> Option<Node> {
    match vnode {
        VNode::Element(element) =>
            Some(render_element(element, instance_key, state, handle)),
        VNode::Component(vcomp) =>
            Some(render_component(vcomp, state, handle)),
        VNode::Text(text) =>
            Some(to_node(&handle.document.create_text_node(&text))),
        VNode::None =>
            None,
    }
}

fn render_element(
    element: &VElement, instance_key: DefaultKey,
    state: &mut State, handle: &BerylHandle,
) -> Node {
    let dom = handle.document.create_element(&element.tag).unwrap();

    // Add attributes
    for (key, value) in &element.attributes {
        dom.set_attribute(&key, &value).unwrap();
    }

    // Add events
    for (event, handler) in &element.events {
        add_event_to_element(&dom, event, handler, instance_key, state, handle);
    }

    // Hydrate children
    for child in &element.children {
        if !element.keyed {
            verify_no_key(child);
        } else {
            // If we have children that aren't keyed, ignore them completely
            if get_key_with_error(child).is_none() {
                continue
            }
        }

        let child_dom = render_node(child, instance_key, state, handle);
        if let Some(child_dom) = child_dom {
            dom.append_child(&child_dom).unwrap();
        }
    }

    to_node(&dom)
}

/// Renders a new component and registers it in the state.
pub fn render_component(
    vcomp: &VComponent,
    state: &mut State, handle: &BerylHandle,
) -> Node {
    // Create the instance itself
    let component = handle.registry.get(vcomp.type_id).unwrap();
    let instance = component.create();

    // Render the instance to a vDOM
    let vroot = match try_render_instance(instance.as_ref()) {
        Ok(vroot) => vroot,
        Err(()) => {
            // Error placeholder so we can do at least something valid
            el("div").into()
        }
    };

    // Store the instance for later access, also remembering its ID
    // The key is needed to create event handlers.
    // The `ActiveComponent` is needed to store event handlers.
    let active = ActiveComponent {
        instance,
        root: None,
        vroot: None,
        handlers: Vec::new(),
    };
    let instance_key = state.add_active(active);

    // For simplicity, we want there to always be a valid root node, therefore None root nodes are
    // not allowed.
    let root = if let VNode::None = vroot {
        error!("Components can't have VNode::None as root node.");

        // For stability we do want to put any kind of node here, as the vDOM isn't aware of failed
        // component renders, and will assume it matches the DOM. Let's just use an empty div.
        to_node(&handle.document.create_element("div").unwrap())
    } else {
        // Render the DOM for the vDOM, passing the ID for handlers looking up this component
        render_node(&vroot, instance_key, state, handle).unwrap()
    };

    // Store the vDOM now that it's applied
    let mut active = state.active_mut(instance_key).unwrap();
    active.root = Some(root.clone());
    active.vroot = Some(vroot);

    root
}


pub fn try_render_instance(instance: &ComponentInstanceGeneric) -> Result<VNode, ()> {
    let result = instance.render();

    match result {
        Ok(vroot) => Ok(vroot),
        Err(error) => {
            error!("Couldn't render component:\n{}", error);
            Err(())
        },
    }
}